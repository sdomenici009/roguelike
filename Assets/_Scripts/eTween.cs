﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public static class eTween 
{
    public enum LerpType { Position, Scale, Rotation, LocalPosition, LocalRotation, CanvasGroupAlpha, ResizeRectTransform };

    public static IEnumerator Lerp(Transform transform, LerpType type, float duration, Vector3 target, AnimationCurve progressCurve)
    {
        float progress = 0;

        Vector3 start = Vector3.zero;
        CanvasGroup canvasGroup = null;
        RectTransform rectTransform = null;

        switch (type)
        {
            case LerpType.Position:
                start = transform.position;
                break;

            case LerpType.LocalPosition:
                start = transform.localPosition;
                break;

            case LerpType.Rotation:
                start = transform.rotation.eulerAngles;
                break;

            case LerpType.LocalRotation:
                start = transform.localRotation.eulerAngles;
                break;

            case LerpType.Scale:
                start = transform.localScale;
                break;

            case LerpType.CanvasGroupAlpha:
                canvasGroup = transform.GetComponent<CanvasGroup>();
                start = new Vector3(canvasGroup.alpha, 0, 0);
                break;

            case LerpType.ResizeRectTransform:
                rectTransform = transform.GetComponent<RectTransform>();
                start = rectTransform.sizeDelta;
                break;

            default: break;
        }

        while (progress < 1)
        {
            float currentProgress = progress;

            if (progressCurve != null)
            {
                currentProgress = progressCurve.Evaluate(progress);
            }
            
            switch (type)
            {
                case LerpType.Position:
                    transform.position = Vector3.Lerp(start, target, currentProgress);
                    break;

                case LerpType.LocalPosition:
                    transform.localPosition = Vector3.Lerp(start, target, currentProgress);
                    break;

                case LerpType.Rotation:
                    transform.rotation = Quaternion.Euler(Vector3.Lerp(start, target, currentProgress));
                    break;

                case LerpType.LocalRotation:
                    transform.localRotation = Quaternion.Euler(Vector3.Lerp(start, target, currentProgress));
                    break;

                case LerpType.Scale:
                    transform.localScale = Vector3.Lerp(start, target, currentProgress);
                    break;

                case LerpType.CanvasGroupAlpha:
                    canvasGroup.alpha = Vector3.Lerp(start, target, currentProgress).x;
                    break;

                case LerpType.ResizeRectTransform:
                    rectTransform.sizeDelta = Vector3.Lerp(start, target, currentProgress);
                    break;

                default: break;
            }

            progress += Time.deltaTime / duration;
            yield return new WaitForEndOfFrame();
        }

        switch (type)
        {
            case LerpType.Position:
                transform.position = target;
                break;

            case LerpType.LocalPosition:
                transform.localPosition = target;
                break;

            case LerpType.Rotation:
                transform.rotation = Quaternion.Euler(target);
                break;

            case LerpType.LocalRotation:
                transform.localRotation = Quaternion.Euler(target);
                break;

            case LerpType.Scale:
                transform.localScale = target;
                break;

            case LerpType.CanvasGroupAlpha:
                canvasGroup.alpha = target.x;
                break;

            case LerpType.ResizeRectTransform:
                rectTransform.sizeDelta = target;
                break;

            default: break;
        }
    }
}
