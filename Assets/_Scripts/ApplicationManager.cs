﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationManager : MonoBehaviour {

    private enum ApplicationState { Menu, InGame }
    private ApplicationState currentState;

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private UIManager uiManager;

    [SerializeField]
    private PageManager pageManager;

    [SerializeField]
    private InputManager inputManager;

    public bool isIdle = true;

    IEnumerator Start () {
        isIdle = false;
        yield return StartCoroutine(uiManager.Initialize());
        yield return StartCoroutine(pageManager.Initialize());
        yield return StartCoroutine(gameManager.Initialize(this, uiManager, pageManager, inputManager));
        isIdle = true;
        currentState = ApplicationState.Menu;  
    }

    public void Update()
    {
        switch(currentState)
        {
            case ApplicationState.Menu:
                if(Input.GetKeyDown(KeyCode.Escape))
                {
                    ExitApplication();
                }
                break;

            case ApplicationState.InGame:
                gameManager.Execute();
                break;

            default: break;
        }
    }

    public void TransitionToInGame()
    {
        if (isIdle && gameManager.isIdle)
        {
            isIdle = false;
            StartCoroutine(gameManager.StartGame());
            StartCoroutine(TransitionToState(ApplicationState.InGame));
        }
    }

    public void TransitionToMenu()
    {
        if (isIdle && gameManager.isIdle)
        {
            isIdle = false;
            gameManager.ClearGame();
            StartCoroutine(TransitionToState(ApplicationState.Menu));
        }
    }

    public void ExitApplication()
    {
        if (isIdle)
        {
            Application.Quit();
        }
    }

    private IEnumerator TransitionToState(ApplicationState newState)
    {
        if (currentState != newState)
        {
            ApplicationState prevState = currentState;
            currentState = newState;

            yield return OnEndState(prevState, newState);
            yield return OnStartState(prevState, newState);
        }
    }

    private IEnumerator OnStartState(ApplicationState prevState, ApplicationState newState)
    {
        switch (newState)
        {
            case ApplicationState.InGame:
                yield return StartCoroutine(uiManager.OnGameUnpause(true));
                isIdle = true;                
                break;

            default: break;
        }
    }

    private IEnumerator OnEndState(ApplicationState prevState, ApplicationState newState)
    {
        switch (prevState)
        {
            case ApplicationState.InGame:
                yield return StartCoroutine(uiManager.OnGamePause(true));
                isIdle = true;
                break;

            default: break;
        }
    }
}
