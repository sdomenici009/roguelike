﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour {

    [SerializeField]
    private RectTransform topLeftBackground, topRightBackground, botRightBackground, botLeftBackground, pauseMenu, startMenu;

    private Vector3 topLeftTarget = new Vector3(-270, 480, 0);
    private Vector3 topRightTarget = new Vector3(270, 480, 0);
    private Vector3 botRightTarget = new Vector3(270, -480, 0);
    private Vector3 botLeftTarget = new Vector3(-270, -480, 0);

    [SerializeField]
    private float pauseScreenAnimationDuration, pauseMenuFadeDuration, startScreenAnimationDuration, startMenuFadeDuration, buttonMovementDuration, buttonScaleDuration;

    [SerializeField]
    private AnimationCurve pauseScreenAnimationCurve, startScreenAnimationCurve, buttonMovementAnimationCurve, buttonScaleAnimationCurve;

    [SerializeField]
    private Text score, endOfScreenScoreText;

	public IEnumerator Initialize()
    {
        SetScore("0");
        yield return null;
    }

    public IEnumerator OnGamePause(bool applicationPause)
    {
        if (applicationPause)
        {
            yield return StartCoroutine(eTween.Lerp(pauseMenu.transform, eTween.LerpType.CanvasGroupAlpha, pauseMenuFadeDuration, new Vector3(0, 0, 0), null));
            pauseMenu.gameObject.SetActive(false);

            startMenu.GetComponent<CanvasGroup>().alpha = 0;
            startMenu.gameObject.SetActive(true);
            yield return StartCoroutine(eTween.Lerp(startMenu.transform, eTween.LerpType.CanvasGroupAlpha, startMenuFadeDuration, new Vector3(1, 0, 0), null));
        }
        else
        {
            topLeftBackground.gameObject.SetActive(true);
            topRightBackground.gameObject.SetActive(true);
            botRightBackground.gameObject.SetActive(true);
            botLeftBackground.gameObject.SetActive(true);

            topLeftBackground.localPosition = topLeftTarget * 3;
            topRightBackground.localPosition = topRightTarget * 3;
            botRightBackground.localPosition = botRightTarget * 3;
            botLeftBackground.localPosition = botLeftTarget * 3;

            StartCoroutine(eTween.Lerp(topLeftBackground.transform, eTween.LerpType.LocalPosition, pauseScreenAnimationDuration, topLeftTarget, pauseScreenAnimationCurve));
            StartCoroutine(eTween.Lerp(topRightBackground.transform, eTween.LerpType.LocalPosition, pauseScreenAnimationDuration, topRightTarget, pauseScreenAnimationCurve));
            StartCoroutine(eTween.Lerp(botRightBackground.transform, eTween.LerpType.LocalPosition, pauseScreenAnimationDuration, botRightTarget, pauseScreenAnimationCurve));
            StartCoroutine(eTween.Lerp(botLeftBackground.transform, eTween.LerpType.LocalPosition, pauseScreenAnimationDuration, botLeftTarget, pauseScreenAnimationCurve));

            yield return new WaitForSeconds(pauseScreenAnimationDuration);

            pauseMenu.GetComponent<CanvasGroup>().alpha = 0;
            pauseMenu.gameObject.SetActive(true);
            yield return StartCoroutine(eTween.Lerp(pauseMenu.transform, eTween.LerpType.CanvasGroupAlpha, pauseMenuFadeDuration, new Vector3(1, 0, 0), null));
        }
    }

    public IEnumerator OnGameUnpause(bool applicationPause)
    {
        if (applicationPause)
        {
            yield return StartCoroutine(eTween.Lerp(startMenu.transform, eTween.LerpType.CanvasGroupAlpha, startMenuFadeDuration, new Vector3(0, 0, 0), null));
            startMenu.gameObject.SetActive(false);
        }
        else
        {
            yield return StartCoroutine(eTween.Lerp(pauseMenu.transform, eTween.LerpType.CanvasGroupAlpha, pauseMenuFadeDuration, new Vector3(0, 0, 0), null));
            pauseMenu.gameObject.SetActive(false);
        }

        StartCoroutine(eTween.Lerp(topLeftBackground.transform, eTween.LerpType.LocalPosition, pauseScreenAnimationDuration, topLeftTarget * 3, pauseScreenAnimationCurve));
        StartCoroutine(eTween.Lerp(topRightBackground.transform, eTween.LerpType.LocalPosition, pauseScreenAnimationDuration, topRightTarget * 3, pauseScreenAnimationCurve));
        StartCoroutine(eTween.Lerp(botRightBackground.transform, eTween.LerpType.LocalPosition, pauseScreenAnimationDuration, botRightTarget * 3, pauseScreenAnimationCurve));
        StartCoroutine(eTween.Lerp(botLeftBackground.transform, eTween.LerpType.LocalPosition, pauseScreenAnimationDuration, botLeftTarget * 3, pauseScreenAnimationCurve));

        yield return new WaitForSeconds(pauseScreenAnimationDuration);

        topLeftBackground.gameObject.SetActive(false);
        topRightBackground.gameObject.SetActive(false);
        botRightBackground.gameObject.SetActive(false);
        botLeftBackground.gameObject.SetActive(false);
    }

    public void SetScore(string scoreText)
    {
        score.text = scoreText;
        endOfScreenScoreText.text = "SCORE: " + scoreText;

        highScore.text = "HIGH SCORE: " + PlayerPrefs.GetInt("highscore").ToString();
    }

    [SerializeField]
    private Text highScore;
}
