﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public enum GameState { Play, Pause }

    private GameState currentState;
    public GameState CurrentState
    {
        get { return currentState; }
    }

    private ApplicationManager applicationManager;
    private UIManager uiManager;
    private PageManager pageManager;
    private InputManager inputManager;

    public bool isIdle = true;

    string pageGrid;

    [SerializeField]
    private Player player;

    private int currentPlayerX, currentPlayerY;

    [SerializeField]
    private AnimationCurve pageRotationAnimationCurve;

    [SerializeField]
    private Image[,] gridSpaces = new Image[8, 6];
    [SerializeField]
    private Transform gridParent;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private float pageRotationDuration;

    private List<PageManager.Page> pages = new List<PageManager.Page>();

    private int currentPage = 0;
    private float pageProgress = 0;
    private Vector3 currentPageStartRotation, currentPageStartScale;

    [SerializeField]
    private float pageSpeedUp;

    [SerializeField]
    private Text comboText;

    private Vector3 targetRotation = new Vector3(-180, 0, 0);
    private Vector3 targetScale = new Vector3(1, -1, 1);

    private float fadeProgress = 0;

    private CanvasGroup currentCanvasGroup0, currentCanvasGroup1;

    private int pageNum = 0;

    private bool dead = false;

    [SerializeField]
    private float flashDuration;
    private float flashProgress = 0;
    private Color startColor = Color.white;
    private Color targetColor = Color.red;

    bool soundPlayed = false;

    [SerializeField]
    private GameObject returnButton, resetButton;

    [SerializeField]
    private ParticleSystem comboSystem;

    private int score = 0;

    [SerializeField]
    private Text multiplierText;

    [SerializeField]
    private Transform overlay;

    private int comboMeter = 0;

    [SerializeField]
    private AudioSource pageDrop;

    public IEnumerator Initialize(ApplicationManager applicationManager, UIManager uiManager, PageManager pageManager, InputManager inputManager)
    {
        this.uiManager = uiManager;
        this.pageManager = pageManager;
        this.applicationManager = applicationManager;
        this.inputManager = inputManager;

        currentState = GameState.Pause;

        pages.Add(pageManager.CreatePage());
        pages.Add(pageManager.CreatePage());

        currentPlayerX = 5;
        currentPlayerY = 2;

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                gridSpaces[i, 5-j] = gridParent.GetChild(i + j * 8).GetComponent<Image>();
            }
        }

        yield return null;
    }

    public void Execute()
    {
        switch (currentState)
        {
            case GameState.Play:
                if (isIdle && Input.GetKeyDown(KeyCode.Escape))
                {
                    StartCoroutine(TransitionToState(GameState.Pause));
                }
                else
                {
                    if (!dead)
                    {
                        FlashCurrentGridSpaceRed();

                        ParseInput();

                        HandlePages();

                        if (!dead)
                        {
                            player.HandleMovement();
                        }
                    }
                }

                break;

            case GameState.Pause:
                if (isIdle && Input.GetKeyDown(KeyCode.Escape))
                {
                    StartCoroutine(TransitionToState(GameState.Play));
                }

                break;

            default: break;
        }   
    }

    private void ParseInput()
    {
        string playerDirection = inputManager.HandleInput();

        bool isValidMove = IsValidMove(playerDirection);

        if (isValidMove)
        {
            gridSpaces[currentPlayerX, currentPlayerY].color = Color.red;

            switch (playerDirection)
            {
                case "left":
                    player.EnqueueMove(new Vector2(-1, 0));
                    currentPlayerX--;
                    break;

                case "right":
                    player.EnqueueMove(new Vector2(1, 0));
                    currentPlayerX++;
                    break;

                case "up":
                    player.EnqueueMove(new Vector2(0, 1));
                    currentPlayerY++;
                    break;
                case "down":
                    player.EnqueueMove(new Vector2(0, -1));
                    currentPlayerY--;
                    break;

                default:
                    break;
            }

            HandleCombo();
        }
    }

    private bool IsValidMove(string move)
    {
        switch(move)
        {
            case "left":
                return currentPlayerX > 0;
            case "right":
                return currentPlayerX < 7;
            case "up":
                return currentPlayerY < 5;
            case "down":
                return currentPlayerY > 0;
            default: return false;
        }
    }

    private void FlashCurrentGridSpaceRed()
    {
        gridSpaces[currentPlayerX, currentPlayerY].color = Color.Lerp(startColor, targetColor, flashProgress);
        flashProgress += Time.deltaTime / flashDuration;

        if(flashProgress >= 1)
        {
            Color temp = startColor;
            startColor = targetColor;
            targetColor = temp;
            flashProgress = 0;
        }
    }

    private IEnumerator RaisePitch(float targetFloat, float duration)
    {
        float progress = 0;
        float startFloat = audioSource.pitch;

        while(progress < 1)
        {
            audioSource.pitch = Mathf.Lerp(startFloat, targetFloat, progress);
            progress += Time.deltaTime / duration;
            yield return null;
        }

        audioSource.pitch = targetFloat;
    }

    private void HandlePages()
    {
        if(pageProgress == 0)
        {
            soundPlayed = false;
            pageNum++;
            if(pageNum != 0 && pageNum % 5 == 0 && pageRotationDuration > 1.4f)
            {
                pageRotationDuration *= pageSpeedUp;
                StartCoroutine(RaisePitch(audioSource.pitch + (audioSource.pitch * (1 - pageSpeedUp)), 1f));
            }
            currentPageStartRotation = pages[currentPage].body.transform.rotation.eulerAngles;
            currentPageStartScale = pages[currentPage].shadow.transform.localScale;
            player.transform.SetSiblingIndex(player.transform.GetSiblingIndex() - 2);
        }

        if (pageProgress < 1)
        {
            pages[currentPage].body.transform.rotation = Quaternion.Euler(Vector3.Lerp(currentPageStartRotation, targetRotation, pageRotationAnimationCurve.Evaluate(pageProgress)));
            pages[currentPage].shadow.transform.localScale = Vector3.Lerp(currentPageStartScale, targetScale, pageRotationAnimationCurve.Evaluate(pageProgress));
            pageProgress += Time.deltaTime / pageRotationDuration;
        }

        if (pageProgress > (pageRotationDuration - .5f) / pageRotationDuration)
        {
            if (!soundPlayed)
            {
                soundPlayed = true;
                pageDrop.Play();
            }
        }

        if (pageProgress >= 1)
        {
            if (fadeProgress == 0)
            {
                if (pageGrid[currentPlayerX + currentPlayerY * 8] != 'E')
                {
                    returnButton.gameObject.SetActive(false);
                    resetButton.gameObject.SetActive(true);

                    StartCoroutine(RaisePitch(1, 1f));
                    dead = true;
                    score += trailLength * comboMeter;

                    player.transform.position = gridSpaces[currentPlayerX, currentPlayerY].transform.position;

                    if (PlayerPrefs.GetInt("highscore") < score)
                    {
                        PlayerPrefs.SetInt("highscore", score);
                    }

                    uiManager.SetScore(score.ToString());
                    StartCoroutine(OnDeath());
                    TriggerCombo();
                }
                else
                {
                    pages[currentPage].body.transform.rotation = Quaternion.Euler(targetRotation);

                    Destroy(pages[currentPage].shadow.gameObject);

                    currentCanvasGroup0 = pages[currentPage].body.GetComponent<CanvasGroup>();
                    pages[currentPage].body.transform.SetSiblingIndex(4 + currentPage * 2);
                }
            }

            if (!dead)
            {
                if (fadeProgress < 1)
                {
                    currentCanvasGroup0.alpha = Mathf.Lerp(1, 0, fadeProgress);
                    fadeProgress += Time.deltaTime / .25f;
                }

                if (fadeProgress >= 1)
                {
                    pages.Add(pageManager.CreatePage());
                    Destroy(pages[currentPage].body.gameObject);
                    pages.RemoveAt(currentPage);
                    pageProgress = 0;
                    fadeProgress = 0;
                    score++;
                    comboMeter++;

                    if (comboMeter > 1)
                        multiplierText.text = "x" + comboMeter;

                    if (PlayerPrefs.GetInt("highscore") < score)
                    {
                        PlayerPrefs.SetInt("highscore", score);
                    }

                    uiManager.SetScore(score.ToString());
                    pageGrid = pages[0].body.name;
                }
            }
        }
    }

    private void TurnOffComboText()
    {
        comboText.gameObject.SetActive(false);
    }

    private void TriggerCombo()
    {
        if (trailLength * comboMeter > 0)
        {
            multiplierText.text = "";
            comboText.text = "+" + (trailLength * comboMeter).ToString();
            comboText.transform.SetParent(player.transform);
            comboText.transform.localPosition = Vector3.zero;
            comboText.transform.SetParent(player.transform.parent);
            comboText.gameObject.SetActive(true);
            Invoke("TurnOffComboText", .5f);
        }
    }

    private IEnumerator OnDeath()
    {
        yield return StartCoroutine(eTween.Lerp(overlay, eTween.LerpType.CanvasGroupAlpha, .175f, new Vector3(1, 0, 0), null));
        yield return StartCoroutine(eTween.Lerp(overlay, eTween.LerpType.CanvasGroupAlpha, .175f, new Vector3(0, 0, 0), null));
        StartCoroutine(TransitionToState(GameState.Pause));
    }

    private void SetAllGridSpacesWhite()
    {
        for(int i=0; i < 8; i++)
        {
            for(int j=0; j < 6; j++)
            {
                gridSpaces[i, j].color = Color.white;
            }
        }
    }
    
    private int trailLength = 0;

    private void HandleCombo()
    {
        flashProgress = 0;
        startColor = Color.white;
        targetColor = Color.red;

        if (gridSpaces[currentPlayerX, currentPlayerY].color == Color.red)
        {
            TriggerCombo();
            SetAllGridSpacesWhite();

            score += trailLength * comboMeter;
            uiManager.SetScore(score.ToString());

            trailLength = 0;
            comboMeter = 0;

            multiplierText.text = "";
        }
        else
        {
            trailLength++;
        }
    }

    public IEnumerator StartGame()
    {
        trailLength = 0;
        comboMeter = 0;
        uiManager.SetScore("0");

        SetAllGridSpacesWhite();

        returnButton.gameObject.SetActive(true);
        resetButton.gameObject.SetActive(false);

        dead = false;

        StartCoroutine(TransitionToState(GameState.Play));

        pages.Add(pageManager.CreatePage());
        currentPage = 0;

        multiplierText.text = "";
        pageGrid = pages[0].body.name;

        currentPageStartRotation = pages[currentPage].body.transform.rotation.eulerAngles;
        currentPageStartScale = pages[currentPage].shadow.transform.localScale;

        yield return null;
    }

    private void ResetGame()
    {
        audioSource.Play();

        trailLength = 0;
        comboMeter = 0;
        SetAllGridSpacesWhite();

        uiManager.SetScore("0");

        returnButton.gameObject.SetActive(true);
        resetButton.gameObject.SetActive(false);

        dead = false;

        pages.Add(pageManager.CreatePage());
        currentPage = 0;

        pageGrid = pages[0].body.name;

        multiplierText.text = "";

        currentPageStartRotation = pages[currentPage].body.transform.rotation.eulerAngles;
        currentPageStartScale = pages[currentPage].shadow.transform.localScale;
    }

    public void ClearGame()
    {
        SetAllGridSpacesWhite();
        StartCoroutine(FadeOutExtraPage(pages[0]));

        pages.RemoveAt(0);

        score = 0;

        pageRotationDuration = 5;

        audioSource.pitch = 1;

        fadeProgress = 0;
        pageProgress = 0;

        player.ResetMovementProgress();

        multiplierText.text = "";
        pageNum = 0;
    }

    private IEnumerator FadeOutExtraPage(PageManager.Page page)
    {
        yield return StartCoroutine(eTween.Lerp(page.body, eTween.LerpType.CanvasGroupAlpha, .25f, new Vector3(0, 0, 0), null));
        yield return StartCoroutine(eTween.Lerp(page.shadow, eTween.LerpType.CanvasGroupAlpha, .25f, new Vector3(0, 0, 0), null));
        Destroy(page.body.gameObject);
        Destroy(page.shadow.gameObject);
    }

    public void TransitionToInGame()
    {
        if (applicationManager.isIdle)
        {
            StartCoroutine(TransitionToState(GameState.Play));
        }
    }

    public IEnumerator TransitionToState(GameState newState)
    {
        if (currentState != newState)
        {
            GameState prevState = currentState;

            if(currentState == GameState.Play)
                currentState = newState;

            yield return OnEndState(prevState, newState);
            yield return OnStartState(prevState, newState);

            if (currentState == GameState.Pause)
                currentState = newState;
        }
    }

    private IEnumerator OnStartState(GameState prevState, GameState newState)
    {
        switch (newState)
        {
            case GameState.Play:
                if(prevState == GameState.Pause && dead)
                {
                    ClearGame();
                    ResetGame();
                }
                break;

            case GameState.Pause:
                if (prevState == GameState.Play)
                {
                    isIdle = false;
                    uiManager.StopAllCoroutines();
                    yield return StartCoroutine(uiManager.OnGamePause(false));
                    isIdle = true;
                }
                break;

            default: break;
        }
    }

    private IEnumerator OnEndState(GameState prevState, GameState newState)
    {
        switch (prevState)
        {
            case GameState.Pause:
                isIdle = false;
                uiManager.StopAllCoroutines();
                yield return StartCoroutine(uiManager.OnGameUnpause(false));
                isIdle = true;     
                break;

            default: break;
        }
    }
}
