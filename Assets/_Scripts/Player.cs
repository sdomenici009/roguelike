﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    [SerializeField]
    private float playerMovementDuration;

    private float playerMovementProgress = 0;
    private Vector3 playerStartPosition;
    private Vector3 playerTarget;

    private bool isPlayerMoving = false;

    private Queue<Vector2> playerMoveQueue = new Queue<Vector2>();

    public void ResetMovementProgress()
    {
        playerMoveQueue.Clear();
        playerMovementProgress = 0;
    }

    public void EnqueueMove(Vector2 move)
    {
        playerMoveQueue.Enqueue(move);
    }

    public void HandleMovement()
    {
        if (playerMoveQueue.Count > 0)
        {
            if (playerMovementProgress == 0)
            {
                playerStartPosition = transform.localPosition;
                Vector2 currentMoveTarget = playerMoveQueue.Peek();
                playerTarget = new Vector3(128 * currentMoveTarget.x, 128 * currentMoveTarget.y, 0);
            }

            transform.localPosition = Vector3.Lerp(playerStartPosition, playerStartPosition + playerTarget, playerMovementProgress);
            playerMovementProgress += Time.deltaTime / playerMovementDuration;

            if (playerMovementProgress >= 1)
            {
                transform.localPosition = playerStartPosition + playerTarget;
                playerMovementProgress = 0;
                playerMoveQueue.Dequeue();
            }
        }
    }
}
