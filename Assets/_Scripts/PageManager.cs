﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class PageManager : MonoBehaviour {

    [SerializeField]
    private GameObject pagePrefab;

    [SerializeField]
    private Transform pageParent;

    [SerializeField]
    private int minCutoutNumber, maxCutoutNumber, minCutoutNeighborDistance;

    [SerializeField]
    private Sprite empty;

    [SerializeField]
    private Sprite[] pageSprites;

    private int pageCount = 0;

    private List<Vector2> previousCutoutList = new List<Vector2>();
    private List<Vector2> currentCutoutList = new List<Vector2>();

    public struct Page
    {
        public Transform body;
        public Transform shadow;

        public Page(Transform body, Transform shadow)
        {
            this.body = body;
            this.shadow = shadow;
        }
    }

    public IEnumerator Initialize()
    {
        yield return null;
    }

    public Page CreatePage()
    {
        RectTransform body = ((GameObject)(Instantiate(pagePrefab))).GetComponent<RectTransform>();
        AddPageToCanvas(body);

        body.transform.localScale = Vector3.one;

        body.name = GenerateGrid(body.transform);

        RectTransform shadowPage = ((GameObject)(Instantiate(body.gameObject))).GetComponent<RectTransform>();
        AddPageToCanvas(shadowPage);

        Transform shadowPageObjectParent = shadowPage.transform.GetChild(0);

        for (int i=0; i < shadowPageObjectParent.childCount; i++)
        {
            shadowPageObjectParent.GetChild(i).GetComponent<Image>().color = new Color(0, 0, 0, .5f);
        }

        shadowPage.transform.localScale = new Vector3(1, 0, 1);

        Page page = new Page(body.transform, shadowPage.transform);
        pageCount += 2;

        return page;
    }

    private void AddPageToCanvas(RectTransform page)
    {
        page.SetParent(pageParent);
        page.SetSiblingIndex(4);

        page.localPosition = Vector3.zero;
        page.anchoredPosition = new Vector3(0, 384, 0);

        page.offsetMin = Vector2.zero;
        page.offsetMax = Vector2.zero;
    }

    private string GenerateGrid(Transform page)
    {
        string pageGrid = "FFFFFFFF" + "FFFFFFFF" + "FFFFFFFF" + "FFFFFFFF" + "FFFFFFFF" + "FFFFFFFF";
        StringBuilder sb = new StringBuilder(pageGrid);

        Transform piecesParent = page.transform.GetChild(0);

        //For each 2x2 piece on the page pick a random 2x2 sprite to set them to.
        for (int i=0; i < 8; i++)
        {
            for(int j=1; j < 6; j++)
            {
                if(i%2 == 0 && (j-1)%2 == 0)
                {
                    int randomSprite = Random.Range(0, pageSprites.Length/4);
                    piecesParent.GetChild(i + j * 8).GetComponent<Image>().sprite = pageSprites[3 + 4* randomSprite];
                    piecesParent.GetChild(i + 1 + j * 8).GetComponent<Image>().sprite = pageSprites[2 + 4* randomSprite];
                    piecesParent.GetChild(i + 1 + (j - 1) * 8).GetComponent<Image>().sprite = pageSprites[1 + 4* randomSprite];
                    piecesParent.GetChild(i + (j - 1) * 8).GetComponent<Image>().sprite = pageSprites[4* randomSprite];
                }
            }
        }

        int currentCutoutCount = 0;
        int cutoutGoal = Random.Range(minCutoutNumber, maxCutoutNumber + 1);
        while(currentCutoutCount < cutoutGoal)
        {
            int randomX = Random.Range(0, 8);
            int randomY = Random.Range(0, 6);

            bool success = true;

            //Checks the current cutout against the previous list of cutouts
            //so that there are never two empty sprites on top of each other.
            for(int i=0; i < previousCutoutList.Count; i++)
            {
                if(previousCutoutList[i].x == randomX &&
                   previousCutoutList[i].y == randomY)
                {
                    success = false;
                }
            }

            //Checks to see if the current cutout is far enough away from other cutouts.
            for(int j=-minCutoutNeighborDistance; j <= minCutoutNeighborDistance; j++)
            {
                for(int k=-minCutoutNeighborDistance; k <= minCutoutNeighborDistance; k++)
                {
                    if((randomX + j) + (randomY + k) * 8 >= 0 &&
                       (randomX + j) + (randomY + k) * 8 < sb.Length)
                    {
                        if (sb[(randomX + j) + (randomY + k) * 8] == 'E')
                        {
                            success = false;
                        }
                    }   
                }
            }
            
            if(success)
            {
                sb[randomX + randomY * 8] = 'E';
                page.transform.GetChild(0).GetChild(randomX + randomY * 8).GetComponent<Image>().sprite = empty;
                currentCutoutCount++;
                currentCutoutList.Add(new Vector2(randomX, randomY));
            }
        }

        previousCutoutList.Clear();

        for (int i = 0; i < currentCutoutList.Count; i++)
        {
            previousCutoutList.Add(currentCutoutList[i]);
        }

        currentCutoutList.Clear();

        pageGrid = sb.ToString();

        return pageGrid;
    }
}
